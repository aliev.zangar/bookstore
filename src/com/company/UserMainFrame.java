package com.company;
import javax.swing.*;
import java.awt.*;

public class UserMainFrame extends JFrame {
    public UserAdd addUser;
    public UserRegistr regitr;
    public UserMainMenu mainMenu;
    public UserViewBook showBook;
    public UserAboutBook about;

    public UserMainFrame() {
        setTitle("USER");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(700, 700);
        setLayout(null);
        setLocationRelativeTo(null);
        addUser = new UserAdd();
        addUser.setLocation(0, 0);
        addUser.setVisible(true);
        add(addUser);

        regitr=new UserRegistr();
        regitr.setLocation(0,0);
        regitr.setVisible(false);
        add(regitr);

        mainMenu=new UserMainMenu();
        mainMenu.setLocation(0,0);
        mainMenu.setVisible(false);
        add(mainMenu);

        showBook=new UserViewBook();
        showBook.setLocation(0,0);
        showBook.setVisible(false);
        add(showBook);

        about=new UserAboutBook();
        about.setLocation(0,0);
        about.setVisible(false);
        add(about);

    }
}



