package com.company;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminAddMagazine extends Container {
    private JLabel name;
    private JTextField textName;
    private JLabel Publishing_House;
    private JTextField textPublishing_House;
    private JLabel labelPrice;
    private JTextField textPrice;
    private JLabel release;
    private JTextField textRelease;
    private JLabel count;
    private JTextField textCount;
    private JLabel genre;
    private JComboBox comboBoxGenre;
    private JButton addBooks;
    private JButton back;


    public AdminAddMagazine() {
        setLayout(null);
        setSize(700, 700);

        name = new JLabel("Name:");
        name.setBounds(250, 150, 80, 40);
        add(name);
        textName = new JTextField();
        textName.setBounds(340, 150, 100, 40);
        add(textName);

        Publishing_House = new JLabel("Publishing_House:");

        Publishing_House.setBounds(250, 200, 80, 40);
        add(Publishing_House);

        textPublishing_House = new JTextField();
        textPublishing_House.setBounds(340, 200, 100, 40);
        add(textPublishing_House);

        labelPrice = new JLabel("Price:");
        labelPrice.setBounds(250, 250, 80, 40);
        add(labelPrice);

        textPrice = new JTextField();
        textPrice.setBounds(340, 250, 100, 40);
        add(textPrice);

        release = new JLabel("release:");
        release.setBounds(250, 300, 80, 40);
        add(release);

        textRelease = new JTextField();
        textRelease.setBounds(340, 300, 100, 40);
        add(textRelease);

        count = new JLabel("Count:");
        count.setBounds(250, 400, 80, 40);
        add(count);

        textCount = new JTextField();
        textCount.setBounds(340, 400, 100, 40);
        add(textCount);

        genre = new JLabel("Genre");
        genre.setBounds(250, 500, 80, 40);
        add(genre);

        comboBoxGenre = new JComboBox();
        comboBoxGenre.setBounds(340, 500, 100, 40);
        comboBoxGenre.addItem("Sport");
        comboBoxGenre.addItem("Teen");
        comboBoxGenre.addItem("Fashion");
        comboBoxGenre.addItem("Health");
        comboBoxGenre.addItem("Music");
        comboBoxGenre.addItem("Football");
        comboBoxGenre.addItem("Gardening");
        comboBoxGenre.addItem("Video games");
        comboBoxGenre.addItem("Memoir");
        add(comboBoxGenre);


        addBooks = new JButton("Add");
        addBooks.setBounds(250, 550, 100, 50);
        addBooks.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                try {

                    //Long id, String product, String genre, String name, int price, int sold, int count, String publishingHouse, int release
                    magazine magazine = new magazine(null,  "Magazine", comboBoxGenre.getSelectedItem().toString(),textName.getText(),Integer.parseInt(textPrice.getText()),Integer.parseInt(textCount.getText()),0, textPublishing_House.getText(),   Integer.parseInt(textRelease.getText()));
                    Admin.addMagazines(magazine);
                    textName.setText("");
                    textCount.setText("");
                    textPrice.setText("");
                    textPublishing_House.setText("");
                    textRelease.setText("");

                    textPrice.setText("");

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        add(addBooks);

        back = new JButton("Back");
        back.setBounds(360, 600, 100, 50);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Admin.frame.addBooks.setVisible(false);
                Admin.frame.listBooks.setVisible(false);
                Admin.frame.deletePage.setVisible(false);
                Admin.frame.addMagazine.setVisible(false);
                Admin.frame.menu.setVisible(true);
                Admin.frame.repaint();
            }
        });
        add(back);
    }
}

