package com.company;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class User {
    public static ObjectOutputStream outputStream;
    public static ObjectInputStream inputStream;
    public static UserMainFrame frame;
    public static Socket  socket;
    public static String accaunt=null;

    private static  ArrayList<UserData> users;

    public static void connectToServer(){
        try{
            socket=new Socket("127.0.0.1",5599);
            outputStream=new ObjectOutputStream(socket.getOutputStream());
            inputStream=new ObjectInputStream((socket.getInputStream()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void addUser(UserData user){
        PackageData pd=new PackageData();
        pd.setOperationType("Add_User");
        pd.setUser(user);
        try {
            outputStream.writeObject(pd);
        }catch (Exception e){e.printStackTrace();}
        }
    public static void addBasket(Basket basket){
        PackageData pd=new PackageData();
        pd.setOperationType("Add_basket");
        pd.setBasket(basket);
        try {
            outputStream.writeObject(pd);
        }catch (Exception e){e.printStackTrace();}
    }

    public static ArrayList<UserData> listUser(){
        ArrayList<UserData> users=new ArrayList<>();
        PackageData pd=new PackageData();
        pd.setOperationType("List_Users");
        pd.setUsers(users);
        try {
            outputStream.writeObject(pd);
            if((pd = (PackageData) inputStream.readObject())!=null){
                users=pd.getUsers();
                for(UserData ud : users)
                    System.out.println(ud);
            }
        }catch (Exception e){e.printStackTrace();}
        return users;
    }
    public static ArrayList<Basket> listBasket(){
        ArrayList<Basket> list=new ArrayList<>();
        PackageData pd=new PackageData();
        pd.setOperationType("List_Baskets");
        pd.setBaskets(list);
        try {
            outputStream.writeObject(pd);
            if((pd = (PackageData) inputStream.readObject())!=null){
                list=pd.getBaskets();

            }
        }catch (Exception e){e.printStackTrace();}
        return list;
    }
    public static ArrayList<book> listBooks(){
        ArrayList<book> books=new ArrayList<>();
        PackageData pd=new PackageData();
        pd.setOperationType("List_Books");
        pd.setBooks(books);
        try {
            outputStream.writeObject(pd);
            if((pd = (PackageData) inputStream.readObject())!=null){
                books=pd.getBooks();
            }
        }catch (Exception e){e.printStackTrace();}
        return books;
    }
    public static void edit(Long id){
        PackageData pd=new PackageData();
        pd.setOperationType("Edit");
        pd.setId(id);
        // ArrayList<book> rooms=pd.getBooks();
        try {
            outputStream.writeObject(pd);
        }catch(Exception e){e.printStackTrace();}
    }
    public static void myLogin(String login){
        accaunt=login;
    }



    public static void showMainMenu() {
        User.frame.addUser.setVisible(false);
        User.frame.regitr.setVisible(false);
        User.frame.mainMenu.setVisible(true);
        User.frame.showBook.setVisible(false);
        User.frame.about.setVisible(false);
        User.frame.repaint();
    }

    public static void showMenuPage() {
     User.frame.addUser.setVisible(true);
     User.frame.regitr.setVisible(false);
     User.frame.mainMenu.setVisible(false);
     User.frame.showBook.setVisible(false);
     User.frame.about.setVisible(false);
     User.frame.repaint();
    }
    public static void showRegistrPage(){
        User.frame.addUser.setVisible(false);
        User.frame.regitr.setVisible(true);
        User.frame.mainMenu.setVisible(false);
        User.frame.showBook.setVisible(false);
        User.frame.about.setVisible(false);
        User.frame.repaint();
    }

    public static void showBook(){

        User.frame.addUser.setVisible(false);
        User.frame.regitr.setVisible(false);
        User.frame.mainMenu.setVisible(false);
        User.frame.showBook.setVisible(true);
        User.frame.about.setVisible(false);
        ArrayList<book> list=listBooks();
        frame.showBook.updateArea(list);
        User.frame.repaint();
    }



    public static void main(String[] args){
        connectToServer();
        users=User.listUser();

        frame = new UserMainFrame();
        frame.setVisible(true);
        ArrayList<Basket> list=listBasket();
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }

    }
}
