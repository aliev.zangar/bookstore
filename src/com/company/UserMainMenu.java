package com.company;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserMainMenu extends Container {
    public JButton addd;
    public JButton exit;

    public UserMainMenu() {
        setSize(700, 700);
        setLayout(null);
        addd = new JButton("Show Books");
        addd.setLocation(200, 150);
        addd.setSize(300, 30);
        addd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                User.showBook();
            }
        });
        add(addd);


        exit = new JButton("Back");
        exit.setBounds(200, 310, 300, 30);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              User.showMenuPage();
            }
        });
        add(exit);

    }
}
