package com.company;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;

public class ServerThread extends Thread {
    private Connection connection;
    private Socket socket;
    ObjectInputStream inputStream;
    ObjectOutputStream outputStream;
    public static Long id = null;

    public ServerThread(Socket socket, Connection connection) {
        this.socket = socket;
        this.connection = connection;
        try {
            inputStream = new ObjectInputStream(this.socket.getInputStream());
            outputStream = new ObjectOutputStream(this.socket.getOutputStream());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        try {
            PackageData pd = null;
            while ((pd = (PackageData) inputStream.readObject()) != null) {

                if (pd.getOperationType().equals("Add_Book")) {
                    try {
                        book book = pd.getBook();
                        addBook(book);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else if (pd.getOperationType().equals("Add_Magazine")) {
                    try {
                        magazine magazine = pd.getMagazines().get(0);
                        addMagazine(magazine);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else if (pd.getOperationType().equals("Add_basket")) {
                    try {
                        Basket user = pd.getBasket();
                        addBasket(user);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else if (pd.getOperationType().equals("List_Books")) {
                    ArrayList<book> rooms = getAllBooks();
                    PackageData resp = new PackageData();
                    resp.setBooks(rooms);
                    outputStream.writeObject(resp);
                }
                else if (pd.getOperationType().equals("List_Baskets")) {
                    ArrayList<Basket> baskets = getAllBaskets();
                    PackageData resp = new PackageData();
                    resp.setBaskets(baskets);
                    outputStream.writeObject(resp);
                }
                else if (pd.getOperationType().equals("List_Users")) {
                    ArrayList<UserData> users = getAllUsers();
                    PackageData resp = new PackageData();
                    resp.setUsers(users);
                    outputStream.writeObject(resp);
                } else if (pd.getOperationType().equals("List_Magazine")) {
                    ArrayList<magazine> magazine = getAllMagazine();
                    PackageData pd2 = new PackageData();
                    pd2.setMagazines(magazine);
                    outputStream.writeObject(pd2);
                }
                else if (pd.getOperationType().equals("Edit")) {
                    Long id=pd.getId();
                    edit(id);
                }
                else if (pd.getOperationType().equals("Delete_Books")) {
                    Long id = pd.getId();
                    deleteBookToDB(id);
                } else if (pd.getOperationType().equals("Add_User")) {
                    try {
                        UserData user = pd.getUser();
                        addUser(user);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addMagazine(magazine magazine) {
        try {
            id = id + 1;
            PreparedStatement ps = connection.prepareStatement("INSERT INTO magazine (id, name,publishingHouse,price,sold,count,releaseM,genre) VALUES(id, ?, ?,?,?,?,?,?)");
            ps.setString(1, magazine.getName());
            ps.setString(2, magazine.getPublishingHouse());
            ps.setInt(3, magazine.getPrice());
            ps.setInt(4, magazine.getSold());
            ps.setString(7, magazine.getGenre());
            ps.setInt(5, magazine.getCount());
            ps.setInt(6, magazine.getRelease());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addBasket(Basket basket) {
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO basket (login,product,price,id) VALUES(?,?,?,?)");
            ps.setString(1, basket.getLogin());
            ps.setString(2, basket.getProduct());
            ps.setInt(3,basket.getPrice());
            ps.setLong(4,basket.getId());
            System.out.println("BASKET");
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addUser(UserData user) {
        try {

            PreparedStatement ps = connection.prepareStatement("INSERT INTO user (login, password,address,telephoneNumber) VALUES(?,?,?,?)");
            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getAddress());
            ps.setString(4, user.getTelephoneNumber());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void addBook(book book) {
        try {
            PreparedStatement ps = connection.prepareStatement("INSERT INTO book (id, name,author,genre,description,price,count,sold,graduate) VALUES(NULL, ?, ?,?,?,?,?,?,?)");
            ps.setString(1, book.getName());
            ps.setString(2, book.getAuthor());
            ps.setString(3, book.getGenre());
            ps.setString(4, book.getDescription());
            ps.setInt(5, book.getPrice());
            ps.setInt(6, book.getCount());
            ps.setInt(7, book.getSold());
            ps.setInt(8, book.getGraduate());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<UserData> getAllUsers() {
        ArrayList<UserData> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * from user");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String login = rs.getString("login");
                String password = rs.getString("password");
                String address = rs.getString("address");
                String telephoneNumber = rs.getString("telephoneNumber");
                list.add(new UserData(login,password,address,telephoneNumber));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }



    public ArrayList<book> getAllBooks(){
        ArrayList<book> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * from book");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Long id = rs.getLong("id");
                String name = rs.getString("name");
                int sold = rs.getInt("sold");
                String author = rs.getString("author");
                String genre=rs.getString("genre");
                int price=rs.getInt("price");
                int count= rs.getInt("count");
                int graduate=rs.getInt("graduate");
                String desc=rs.getString("description");
                list.add(new book(id,name,"Book",sold,genre,count,price,author,graduate,desc));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    public ArrayList<Basket> getAllBaskets(){
        ArrayList<Basket> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * from basket");
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Long id = rs.getLong("id");
                String name = rs.getString("product");
                int sold = rs.getInt("price");
                String author = rs.getString("login");

                list.add(new Basket(id,author,sold,name));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<magazine> getAllMagazine(){
        ArrayList<magazine> list = new ArrayList<>();

        try {
            PreparedStatement ps = connection.prepareStatement("SELECT * from magazine");

            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                Long id = rs.getLong("id");
                String name = rs.getString("name");
                int sold = rs.getInt("sold");
                String pbH = rs.getString("publishingHouse");
                String genre=rs.getString("genre");
                int price=rs.getInt("price");
                int count= rs.getInt("count");
                int relaese=rs.getInt("relaseM");


                 list.add(new magazine(id,"Magazine",genre, name,price,sold,count,pbH,relaese));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
          return list;
    }


    public void deleteBookToDB(Long id){
        try{
            PreparedStatement ps=connection.prepareStatement("DELETE FROM book WHERE id=?");
            ps.setLong(1,id);
            int rows= ps.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void edit (Long id){


        String update = "UPDATE book SET count=count-1 ,sold = sold+1 WHERE id = ? ";
        try {
            PreparedStatement ps = connection.prepareStatement(update);

            ps.setLong(1,id);
            ps.executeUpdate();

            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteMagazineToDB(Long id){
        try{
            PreparedStatement ps=connection.prepareStatement("DELETE FROM magazine WHERE id=?");
            ps.setLong(1,id);
            int rows= ps.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
