package com.company;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class UserViewBook extends Container {
    private JTable table;
    private JLabel label;
    private JLabel label2;
    private JTextField textField;
    private JButton buy;
    private JButton basket;
    private JButton back;
    private JButton about;

    public UserViewBook(){
        setLayout(null);
        setSize(700,700);

        label=new JLabel("INSERT ID:");
        label.setBounds(250,450,80,30);
        add(label);

        textField=new JTextField("");
        textField.setBounds(340,450,100,30);
        add(textField);
        label2 = new JLabel("");
      label2.setBounds(150,600,100,50);
      add(label2);

        buy=new JButton("Buy");
        buy.setBounds(150,500,100,50);
        buy.addActionListener(new ActionListener() {
            @Override

                public void actionPerformed(ActionEvent e) {
                   Long id;
                    try{

                        id=Long.parseLong(textField.getText());

                       User.edit(id);
                        label2.setText("Thank you");
                        textField.setText("");

                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
        });
        add(buy);

        back = new JButton("BACK");
        back.setBounds(500,600,100,50);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                User.showMainMenu();
            }
        });
        add(back);


        about=new JButton("About_Book");
        about.setBounds(260,500,180,50);
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null,
                        "Про книгу детально спросите у админа!!! ");
            }
        });
        add(about);

    }

    public void updateArea(ArrayList<book> books){

        String columnNames[] =new String[]{"ID",  "NAME", "AUTHOR","PRICE"};

        String data[][] = new String[books.size()][9];

        int i = 0;
        for(book st : books){
            data[i][0] = String.valueOf(st.getId());
            data[i][1] = st.getName();
            data[i][2] = st.getAuthor();
            data[i][3] = String.valueOf(st.getPrice());
            i++;
        }

        table = new JTable(data, columnNames);
        table.setBounds(70,80,500,500);

        add(table);
        repaint();
    }

}
