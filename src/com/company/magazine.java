package com.company;

import java.io.Serializable;

public class magazine extends bookstore implements Serializable {
    private String publishingHouse;
    private int   release;


    public magazine() {

    }

    public magazine(Long id, String product, String genre, String name, int price, int sold, int count, String publishingHouse, int release) {
        super(id, product, genre, name, price, sold, count);
        this.publishingHouse = publishingHouse;
        this.release = release;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public int getRelease() {
        return release;
    }

    public void setRelease(int release) {
        this.release = release;
    }

    @Override
    public String toString() {
        return "magazine{" +
                "publishingHouse='" + publishingHouse + '\'' +
                ", release=" + release +
                '}';
    }
}
