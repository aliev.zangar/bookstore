package com.company;

import java.io.*;
import java.util.ArrayList;

public class PackageData implements Serializable {
    String operationType;
    ArrayList<book> books;
    ArrayList<magazine> magazines;
    UserData user;
    ArrayList<UserData> users;
    ArrayList<Basket> baskets;
    Basket basket;
    book book;
    magazine magaz;
    Long id;


    public PackageData() {
    }

    public PackageData(String operationType, UserData user, ArrayList<UserData> users, ArrayList<com.company.book> books, ArrayList<magazine> magazines, com.company.book book, magazine magaz, Long id) {
        this.operationType = operationType;
        this.books = books;
        this.magazines = magazines;
        this.book = book;
        this.magaz = magaz;
        this.id = id;
        this.user=user;
        this.users=users;

    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public ArrayList<com.company.book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<com.company.book> books) {
        this.books = books;
    }

    public ArrayList<magazine> getMagazines() {
        return magazines;
    }

    public void setMagazines(ArrayList<magazine> magazines) {
        this.magazines = magazines;
    }

    public com.company.book getBook() {
        return book;
    }

    public void setBook(com.company.book book) {
        this.book = book;
    }

    public magazine getMagaz() {
        return magaz;
    }

    public void setMagaz(magazine magaz) {
        this.magaz = magaz;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }

    public ArrayList<UserData> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserData> users) {
        this.users = users;
    }

    public ArrayList<Basket> getBaskets() {
        return baskets;
    }

    public void setBaskets(ArrayList<Basket> baskets) {
        this.baskets = baskets;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    @Override
    public String toString() {
        return "PackageData{" +
                "operationType='" + operationType + '\'' +
                ", books=" + books +
                ", magazines=" + magazines +
                ", user=" + user +
                ", users=" + users +
                ", baskets=" + baskets +
                ", basket=" + basket +
                ", book=" + book +
                ", magaz=" + magaz +
                ", id=" + id +
                '}';
    }
}
