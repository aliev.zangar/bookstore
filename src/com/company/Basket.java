package com.company;

import java.io.Serializable;

public class Basket implements Serializable {
    private Long id;
    private String login;
    private int price;
    private String product;

    public Basket() {
    }

    public Basket(Long id, String login, int price, String product) {
        this.id = id;
        this.login = login;
        this.price = price;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Basket{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", price=" + price +
                ", product='" + product + '\'' +
                '}';
    }
}
